-- 1 task
-- 1 solution
SELECT s.store_id, s.staff_id, SUM(p.amount) as sum_amount
FROM staff s
JOIN payment p ON s.staff_id = p.staff_id
WHERE EXTRACT(YEAR FROM p.payment_date) = 2017
GROUP BY s.store_id, s.staff_id
HAVING SUM(p.amount) = (
    SELECT MAX(sum_amount)
    FROM (
        SELECT s.store_id, s.staff_id, SUM(p.amount) as sum_amount
        FROM staff s
        JOIN payment p ON s.staff_id = p.staff_id
        WHERE EXTRACT(YEAR FROM p.payment_date) = 2017
        GROUP BY s.store_id, s.staff_id
    ) results
    WHERE s.store_id = results.store_id
    GROUP BY results.store_id)
ORDER BY s.store_id;

-- 2 solution
SELECT store_id, staff_id, sum_amount1
FROM (
    SELECT
        store_id,
        s1.staff_id,
        SUM(amount) AS sum_amount1,
        ROW_NUMBER() OVER (PARTITION BY store_id ORDER BY SUM(amount) DESC) AS rn
    FROM staff s1
    JOIN payment p1 ON s1.staff_id = p1.staff_id
    WHERE EXTRACT(YEAR FROM payment_date) = 2017
    GROUP BY store_id, s1.staff_id
) AS ranked_data
WHERE rn = 1
ORDER BY store_id;

-- 2 task
-- 1 solution
SELECT film.film_id, b.film_count, film.rating
FROM film
INNER JOIN (
  SELECT inventory.film_id, SUM(a.cnt) as film_count
  FROM inventory
  INNER JOIN (
    SELECT inventory_id, COUNT(inventory_id) as cnt
    FROM rental
    GROUP BY inventory_id
  ) AS a
  ON inventory.inventory_id = a.inventory_id
  GROUP BY film_id
) AS b
ON film.film_id = b.film_id
ORDER BY b.film_count DESC, film_id
LIMIT 5;

-- 2 solution
SELECT film.film_id, COALESCE(rental_counts.film_count, 0) AS film_count, film.rating
FROM film
LEFT JOIN (
  SELECT i.film_id, COUNT(r.inventory_id) AS film_count
  FROM inventory i
  LEFT JOIN rental r ON i.inventory_id = r.inventory_id
  GROUP BY i.film_id
) AS rental_counts
ON film.film_id = rental_counts.film_id
ORDER BY film_count DESC, film_id
LIMIT 5;


-- 3 task 
-- 1 solution
SELECT MAX(release_year), actor.actor_id
FROM film
INNER JOIN (
	SELECT actor_id, film_id
	FROM film_actor
) AS actor
ON film.film_id = actor.film_id
GROUP BY actor.actor_id
ORDER BY 1;

-- 2 solution
SELECT MAX(film.release_year) AS max_release_year, film_actor.actor_id
FROM film_actor
INNER JOIN film ON film_actor.film_id = film.film_id
GROUP BY film_actor.actor_id
ORDER BY max_release_year;


